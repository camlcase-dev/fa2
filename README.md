# FA2

- [tzip-12: FA2 specification](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md)
- [tzip-16: token metadata specification](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-16/tzip-16.md)

Simple implementation of FA2 in ligo.


Token Metadata
`(map string bytes)`

```bash
$ printf "Hello World!" | od -t x1 -w1000 -A n | tr -d ' '
48656c6c6f20576f726c6421

tobytes () {
  printf 0x
  cat | od -t x1 -w1000 -A n | tr -d ' '
}
$ printf "AABB" | tobytes
0x41414242

ofbytes () {
   while read -N2 code; do
     case "$code" in 0x ) ;; * ) printf "\x$code" ;; esac ;
   done
}

$ echo 0x41414242 | ofbytes
AABB
```
