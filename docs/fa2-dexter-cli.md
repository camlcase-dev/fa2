## Originate Dexter FA2 contract 

```
tezos-client originate contract tezosTreasureExchange transferring 0 from alicia running ./morley/dexter-fa2.tz --init 'Pair {} (Pair (Pair False (Pair False 0)) (Pair (Pair "tz1d99SzFV8uxA7WsaEpiCY5UGjyS6t85Yc5" (Pair "KT1N12jus7e2WghvbrbKfgNKAjtUDY6TFesQ" 0)) (Pair 0 0)))' --burn-cap 20 --force
```

tezosTreasureExchange
KT1XcVGHKpjRbTVZMLtiruk5ygSw6k5V6WuG

## Add liquidity
```
tezos-client transfer 10 from alicia to tezosTreasureExchange --entrypoint 'addLiquidity' --arg 'Pair (Pair "tz1d99SzFV8uxA7WsaEpiCY5UGjyS6t85Yc5" 1) (Pair 1000000000 "2030-01-01T12:00:00Z")' --burn-cap 1
```

## XTZ to Token
```
tezos-client transfer 5 from brandon to tezosTreasureExchange --entrypoint 'xtzToToken' --arg 'Pair "tz1QA4dcZJYxqmciD4HKXKPvKW1vSJy2e5bX" (Pair 1 "2030-01-29T18:00:00Z")'  --burn-cap 1
```

## Token to Token
```
tezos-client transfer 0 from alicia to tezosTreasureExchange --entrypoint 'tokenToToken' --arg 'Pair (Pair "KT1B7U9EfmxKNtTmwzPZjZuRwRy8JCfPW5VS" (Pair 1 "tz1d99SzFV8uxA7WsaEpiCY5UGjyS6t85Yc5")) (Pair "tz1d99SzFV8uxA7WsaEpiCY5UGjyS6t85Yc5" (Pair 1000000 "2030-01-01T12:00:00Z"))'  --burn-cap 1
```
