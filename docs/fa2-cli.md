## Originate FA2 contract

```
tezos-client originate contract tezosTreasure transferring 0 from alicia running ./morley/fa2.tz --init 'Pair {Elt (Pair "tz1d99SzFV8uxA7WsaEpiCY5UGjyS6t85Yc5" 0) 1000000000000} (Pair {} {Elt 0 (Pair 0 {Elt "decimals" 0x36; Elt "name" 0x5472656173757265; Elt "symbol" 0x545352})})' --burn-cap 5
```

Treasure TSR
KT1N12jus7e2WghvbrbKfgNKAjtUDY6TFesQ

## Set Dexter contract as Alicia's operator
```
tezos-client transfer 0 from alicia to tezosTreasure --entrypoint 'update_operators' --arg '{Left (Pair "tz1d99SzFV8uxA7WsaEpiCY5UGjyS6t85Yc5" (Pair "KT1XcVGHKpjRbTVZMLtiruk5ygSw6k5V6WuG" 0))}' --burn-cap 1
```

## Transfer
```
tezos-client transfer 0 from alicia to tezosTreasure --entrypoint 'transfer' --arg '{Pair "tz1d99SzFV8uxA7WsaEpiCY5UGjyS6t85Yc5" {Pair "tz1QA4dcZJYxqmciD4HKXKPvKW1vSJy2e5bX" (Pair 0 1000)}}' --burn-cap 1
```
