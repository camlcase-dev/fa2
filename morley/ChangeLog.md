# Changelog for fa-two

# 0.3.0.0 -- 2021-02-06
* Update morley version to commit 913ff3c25ddf9688ac140ce218ac17c9bb5f8753.
* Fix tests.

# 0.2.0.0 -- 2021-01-14
* Import the LIGO version of the contract.
* Run the unit and property tests on both versions of the contract.
* Fix minor error message bug in transfer for morley version.

# 0.1.0.0 -- 2021-01-13
* Implement a simple FA2 contract.
* Implement unit and property tests.
