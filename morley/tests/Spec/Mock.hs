{-|
Module      : Spec.Mock
Copyright   : (c) camlCase, 2021
Maintainer  : james@camlcase.io

Mock values that are useful for testing.

-}

{-# LANGUAGE OverloadedStrings      #-}

module Spec.Mock where

import Tezos.Address (Address(..), mkKeyAddress)
import Tezos.Crypto (parsePublicKey)

-- =============================================================================
-- contracts
-- =============================================================================

token :: Address
token =
  case parsePublicKey "edpkubykerhPZDK441ZT3gdKNoCNBKcKu1V4ghwAJfrcZujifkyPM1" of
    Right key -> mkKeyAddress key
    _ -> error "alice: unable to parse public key."

-- =============================================================================
-- accounts
-- =============================================================================

alice :: Address
alice =
  case parsePublicKey "edpkvQiPK3HQgjMRe3e7tZDoRVCYvpq3VzmheymL2vy6JWcdugvKSv" of
    Right key -> mkKeyAddress key
    _ -> error "alice: unable to parse public key."

bob :: Address
bob =
  case parsePublicKey "edpkucSSDmuEXgmSPXWt1mL7wzFGM2VtyjV1WA8ymi5AE7ZF5efmWr" of
    Right key -> mkKeyAddress key
    _ -> error "bob: unable to parse public key."

simon :: Address
simon =
  case parsePublicKey "edpktyjLG3cpMMeXs8DoR2wh2N22NccSRRsXkJKGWyQjJ3y8qDQE2X" of
    Right key -> mkKeyAddress key
    _ -> error "simon: unable to parse public key."

camlCase :: Address
camlCase =
  case parsePublicKey "edpkty3xFUugRLM3Ki3Pk6D9iotzE4rx9hu5G1DGNCeAeUFpZd5dNq" of
    Right key -> mkKeyAddress key
    _ -> error "camlCase: unable to parse public key."

tezosTacosFA2 :: Address
tezosTacosFA2 =
  case parsePublicKey "edpkunywQpkYkHEYa3iTCszvnMKnu1ou3ekkeMHYbjCeR5KeDt7h8c" of
    Right key -> mkKeyAddress key
    _ -> error "tezosTacosFA12: unable to parse public key."
