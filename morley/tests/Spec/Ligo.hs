{-# LANGUAGE AllowAmbiguousTypes       #-}
{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Spec.Ligo where

-- fa2
import           FaTwo.Core          as FA
import           FaTwo.Ligo.Contract (fa2Contract)

import qualified Spec.Core as Core
-- lorentz and morley
import           Lorentz        (Mutez, TAddress(..), toVal)
import           Lorentz.Test
import           Michelson.Test (tOriginate)

-- testing
import           Test.Hspec      (Spec)

originate
  :: FA.Storage
  -> Mutez
  -> IntegrationalScenarioM (TAddress FA.Parameter)
originate storage mutez =
  TAddress @Parameter <$> tOriginate fa2Contract "FA2" (toVal storage) mutez
  
spec :: Spec
spec = Core.spec "Ligo FA2 Contract" originate
