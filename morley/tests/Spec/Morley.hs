{-# LANGUAGE OverloadedStrings         #-}

module Spec.Morley where

-- fa2
import qualified FaTwo.Core     as FA
import qualified FaTwo.Contract as FA

-- lorentz and morley
import           Lorentz      (Mutez, TAddress) 
import           Lorentz.Test

import qualified Spec.Core as Core
import           Test.Hspec (Spec)

originate
  :: FA.Storage
  -> Mutez
  -> IntegrationalScenarioM (TAddress FA.Parameter)
originate storage mutez = do
  lOriginate FA.contract "FA2" storage mutez

spec :: Spec
spec = Core.spec "Morley FA2 Contract" originate
