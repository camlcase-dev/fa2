{-|
Module      : Test.Transfer
Copyright   : (c) camlCase, 2021
Maintainer  : james@camlcase.io

Unit and property tests for the transfer entrypoint in FA2.

-}

{-# LANGUAGE AllowAmbiguousTypes       #-}
{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module Spec.Transfer where

-- fa2
import qualified FaTwo.Test     as FA
import qualified FaTwo.Contract as FA
import qualified Spec.Mock      as Mock

-- lorentz and morley
import           Lorentz         ( Address, Mutez, IsoValue, ToTAddress, mt, zeroMutez) 
import           Lorentz.Entrypoints
import           Lorentz.Test
import           Util.Named ((.!))
import           Michelson.Test.Integrational (integrationalTest)

-- string
import Fmt (pretty)

-- testing
import           Test.Hspec      (Spec, it)
import           Test.QuickCheck (Gen, Property, choose, forAll)
import qualified Test.QuickCheck.Property as QC


spec :: Spec
spec = do
  it "unit: transfer to self does not change the storage" $ do
    integrationalTestExpectation $ do
      let storage = FA.initStorage Mock.alice 1000000
      fa <- lOriginate FA.contract "FA2" storage zeroMutez

      lCallEPWithMutez Mock.alice fa (Call @"Transfer") [FA.mkTransfer Mock.alice [(Mock.alice, 0, 100)]] zeroMutez

      lExpectStorageConst fa storage

  it "unit: transfer to another address decreases the owner's balance and increases the receiver's balance" $ do
    integrationalTestExpectation $ do
      let storage = FA.initStorage Mock.alice 1000000
      fa <- lOriginate FA.contract "FA2" storage zeroMutez

      lCallEPWithMutez Mock.alice fa (Call @"Transfer") [FA.mkTransfer Mock.alice [(Mock.bob, 0, 100)]] zeroMutez

      let expectedStorage =
            FA.insertBalance Mock.alice 999900 $
            FA.insertBalance Mock.bob 100
            storage

      lExpectStorageConst fa expectedStorage

  it "unit: perform multiple transfers from one owner" $ do
    integrationalTestExpectation $ do
      let storage = FA.initStorage Mock.alice 1000000
      fa <- lOriginate FA.contract "FA2" storage zeroMutez

      lCallEPWithMutez Mock.alice fa (Call @"Transfer")
        [FA.mkTransfer Mock.alice [(Mock.bob, 0, 100), (Mock.simon, 0, 100)]] zeroMutez

      let expectedStorage =
            FA.insertBalance Mock.alice 999800 $
            FA.insertBalance Mock.bob 100 $
            FA.insertBalance Mock.simon 100 $            
            storage

      lExpectStorageConst fa expectedStorage

  it "unit: set third party as operator" $ do
    integrationalTestExpectation $ do
      let storage = FA.initStorage Mock.alice 1000000
      fa <- lOriginate FA.contract "FA2" storage zeroMutez

      lCallEPWithMutez Mock.alice fa (Call @"Update_operators") [FA.mkAdd_operator Mock.alice Mock.bob 0] zeroMutez

      let expectedStorage =
            FA.insertOperator Mock.alice Mock.bob 0
            storage

      lExpectStorageConst fa expectedStorage

  it "unit: set third party as operator then remove " $ do
    integrationalTestExpectation $ do
      let storage = FA.initStorage Mock.alice 1000000
      fa <- lOriginate FA.contract "FA2" storage zeroMutez

      lCallEPWithMutez Mock.alice fa (Call @"Update_operators") [FA.mkAdd_operator Mock.alice Mock.bob 0] zeroMutez

      let expectedStorage =
            FA.insertOperator Mock.alice Mock.bob 0
            storage

      lExpectStorageConst fa expectedStorage

      lCallEPWithMutez Mock.alice fa (Call @"Update_operators") [FA.mkRemove_operator Mock.alice Mock.bob 0] zeroMutez

      lExpectStorageConst fa storage

  it "unit: perform multiple transfers from multiple owners with operator" $ do
    integrationalTestExpectation $ do
      let storage = FA.insertBalance Mock.bob 1000000 $ FA.initStorage Mock.alice 1000000
      fa <- lOriginate FA.contract "FA2" storage zeroMutez

      lCallEPWithMutez Mock.alice fa (Call @"Update_operators") [FA.mkAdd_operator Mock.alice Mock.bob 0] zeroMutez

      lCallEPWithMutez Mock.bob fa (Call @"Transfer")
        [FA.mkTransfer Mock.alice [(Mock.simon, 0, 200)],
         FA.mkTransfer Mock.bob [(Mock.simon, 0, 2000)]] zeroMutez

      let expectedStorage =
            FA.insertOperator Mock.alice Mock.bob 0 $
            FA.insertBalance Mock.alice 999800 $
            FA.insertBalance Mock.bob 998000 $
            FA.insertBalance Mock.simon 2200 $            
            storage

      lExpectStorageConst fa expectedStorage

  it "unit: third party operator can transfer owner's tokens" $ do
    integrationalTestExpectation $ do
      let storage = FA.initStorage Mock.alice 1000000
      fa <- lOriginate FA.contract "FA2" storage zeroMutez

      lCallEPWithMutez Mock.alice fa (Call @"Update_operators") [FA.mkAdd_operator Mock.alice Mock.bob 0] zeroMutez
      lCallEPWithMutez Mock.bob fa (Call @"Transfer") [FA.mkTransfer Mock.alice [(Mock.simon, 0, 20000)]] zeroMutez

      let expectedStorage =
            FA.insertOperator Mock.alice Mock.bob 0 $
            FA.insertBalance Mock.alice 980000 $
            FA.insertBalance Mock.simon 20000 $
            storage

      lExpectStorageConst fa expectedStorage

  it "unit: an owner can transfer all of their funds" $ do
    integrationalTestExpectation $ do
      let storage = FA.initStorage Mock.alice 1000000
      fa <- lOriginate FA.contract "FA2" storage zeroMutez

      lCallEPWithMutez Mock.alice fa (Call @"Transfer") [FA.mkTransfer Mock.alice [(Mock.simon, 0, 1000000)]] zeroMutez

      let expectedStorage =
            FA.insertBalance Mock.alice 0 $            
            FA.insertBalance Mock.simon 1000000 $
            storage

      lExpectStorageConst fa expectedStorage

  it "unit: an owner cannot transfer more funds than they own" $ do
    integrationalTestExpectation $ do
      let storage = FA.initStorage Mock.alice 1000000
      fa <- lOriginate FA.contract "FA2" storage zeroMutez

      lCallEPWithMutez Mock.alice fa (Call @"Transfer") [FA.mkTransfer Mock.alice [(Mock.simon, 0, 1000001)]] zeroMutez
        `catchExpectedError` lExpectFailWith (== [mt|FA2_INSUFFICIENT_BALANCE|])

  it "unit: an owner cannot transfer token_ids that don't exist" $ do
    integrationalTestExpectation $ do
      let storage = FA.initStorage Mock.alice 1000000
      fa <- lOriginate FA.contract "FA2" storage zeroMutez

      lCallEPWithMutez Mock.alice fa (Call @"Transfer") [FA.mkTransfer Mock.alice [(Mock.simon, 1, 10)]] zeroMutez
        `catchExpectedError` lExpectFailWith (== [mt|FA2_INSUFFICIENT_BALANCE|])

  it "unit: a non-operator cannot transfer an owner's funds" $ do
    integrationalTestExpectation $ do
      let storage = FA.initStorage Mock.alice 1000000
      fa <- lOriginate FA.contract "FA2" storage zeroMutez

      lCallEPWithMutez Mock.bob fa (Call @"Transfer") [FA.mkTransfer Mock.alice [(Mock.simon, 0, 20000)]] zeroMutez
        `catchExpectedError` lExpectFailWith (== [mt|FA2_NOT_OPERATOR|])

  it "prop: an operator cannot spend more than the owner's funds" $ do
    forAll genGreaterAndLesser $ \(greater, lesser) ->
      integrationalTestProperty $ do
    
        let storage = FA.initStorage Mock.alice lesser
        fa <- lOriginate FA.contract "FA2" storage zeroMutez

        lCallEPWithMutez Mock.alice fa (Call @"Update_operators") [FA.mkAdd_operator Mock.alice Mock.bob 0] zeroMutez
        
        lCallEPWithMutez Mock.bob fa (Call @"Transfer") [FA.mkTransfer Mock.alice [(Mock.bob, 0, greater)]] zeroMutez
          `catchExpectedError` lExpectFailWith (== [mt|FA2_INSUFFICIENT_BALANCE|])

  it "prop: an operator can spend less than or equal to the owner's funds" $ do
    forAll genLessThanOrEqualAndBalance $ \(lessThanOrEqual, balance_) ->
      integrationalTestProperty $ do
    
        let storage = FA.initStorage Mock.alice balance_
        fa <- lOriginate FA.contract "FA2" storage zeroMutez

        lCallEPWithMutez Mock.alice fa (Call @"Update_operators") [FA.mkAdd_operator Mock.alice Mock.bob 0] zeroMutez
        
        lCallEPWithMutez Mock.bob fa (Call @"Transfer") [FA.mkTransfer Mock.alice [(Mock.bob, 0, lessThanOrEqual)]] zeroMutez

        let expectedStorage =
              FA.insertOperator Mock.alice Mock.bob 0 $
              FA.insertBalance Mock.alice (balance_ - lessThanOrEqual) $
              FA.insertBalance Mock.bob lessThanOrEqual $
              storage

        lExpectStorageConst fa expectedStorage

-- | Send mutez from the genesisAddress
lCallEPWithMutez
  :: forall cp epRef epArg toAddr.
     (HasEntrypointArg cp epRef epArg, IsoValue epArg, ToTAddress cp toAddr)
  => Address -> toAddr -> epRef -> epArg -> Mutez -> IntegrationalScenarioM ()
lCallEPWithMutez fromAddr toAddr epRef param mutez =
  lTransfer @cp @epRef @epArg
    (#from .! fromAddr) (#to .! toAddr)
    mutez epRef param

lCallDefWithMutez
  :: forall cp defEpName defArg toAddr.
     ( HasDefEntrypointArg cp defEpName defArg
     , IsoValue defArg
     , ToTAddress cp toAddr
     )
  => Address -> toAddr -> defArg -> Mutez -> IntegrationalScenarioM ()
lCallDefWithMutez fromAddr toAddr mutez =
  lCallEPWithMutez @cp @defEpName @defArg fromAddr toAddr CallDefault mutez

genGreaterAndLesser :: Gen (Natural, Natural)
genGreaterAndLesser = do
  x <- choose (2, 99999999999999) :: Gen Integer
  y <- choose (0, x-1)
  pure (fromIntegral x, fromIntegral y)

genLessThanOrEqualAndBalance :: Gen (Natural, Natural)
genLessThanOrEqualAndBalance = do
  balance_ <- choose (2, 99999999999999) :: Gen Integer
  lessThanOrEqual <- choose (0, balance_)
  pure (fromIntegral lessThanOrEqual, fromIntegral balance_)

integrationalTestProperty :: IntegrationalScenario -> Property
integrationalTestProperty =
  integrationalTest (maybe succeededProp (failedProp . pretty))

succeededProp :: QC.Property
succeededProp = QC.property True

failedProp :: Text -> QC.Property
failedProp r = QC.property $ QC.failed { QC.reason = toString r }
