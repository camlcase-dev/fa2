{-|
Module      : Main
Copyright   : (c) camlCase, 2021
Maintainer  : james@camlcase.io
-}

module Main where

import qualified Spec.Ligo   as Ligo
import qualified Spec.Morley as Morley
import           Test.Hspec  (hspec, parallel)

main :: IO ()
main = do
  hspec $ do
    parallel Morley.spec
    parallel Ligo.spec
