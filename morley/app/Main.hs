{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified Data.Map as Map
import           Lorentz.ContractRegistry
import qualified FaTwo.Contract as FA

main :: IO ()
main = do
  let contractRegistry =
        ContractRegistry $ Map.fromList
        [ "fa2" ?:: ContractInfo
          { ciContract      = FA.contract
          , ciIsDocumented  = True
          , ciStorageParser = Nothing
          , ciStorageNotes  = Nothing
          }    
        ]

  runContractRegistry contractRegistry (Print (Just "fa2") (Just "fa2.tz") False False)


-- tezos-client typecheck script fa2.tz
