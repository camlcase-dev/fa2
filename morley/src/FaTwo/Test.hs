{-|
Module      : FaTwo.Test
Copyright   : (c) camlCase, 2021

Values and functions to test the FA2 contract.

-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE UndecidableInstances      #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}

module FaTwo.Test where

import qualified Data.Map.Strict as Map
import qualified FaTwo.Core as FA
import           Lorentz hiding (amount, balance)
import           Util.Named ((.!))
import           Prelude (fmap)

-- =============================================================================
-- Make parameter values
-- =============================================================================

mkTransferDestination :: (Address, Natural, Natural) -> FA.TransferDestination
mkTransferDestination (to_, token_id, amount) =
  ( (#to_      .! to_)
  , (#token_id .! (#token_id .! token_id))
  , (#amount   .! amount)
  )  

mkTransfer :: Address -> [(Address, Natural, Natural)] -> FA.Transfer
mkTransfer from_ txs =
  ( (#from_    .! from_)
  , (#txs      .! (fmap mkTransferDestination txs))
  )  

mkBalanceOfRequest :: (Address, Natural) -> FA.BalanceOfRequest
mkBalanceOfRequest (owner, token_id) =
  ( #owner    .! owner
  , #token_id .! (#token_id .! token_id)
  )

mkBalanceOfResponse :: Address -> Natural -> Natural -> FA.BalanceOfResponse
mkBalanceOfResponse owner token_id balance =
  ( #request .! mkBalanceOfRequest (owner, token_id)
  , #balance .! balance
  )

mkBalanceOfParam :: [(Address, Natural)] -> ContractRef [FA.BalanceOfResponse] -> FA.BalanceOfParam
mkBalanceOfParam requests callback =
  ( #requests .! (fmap mkBalanceOfRequest requests)
  , #callback .! callback
  )  

mkAdd_operator :: Address -> Address -> Natural -> FA.UpdateOperator
mkAdd_operator owner operator token_id =
  FA.Add_operator
  ( #owner    .! owner
  , #operator .! operator
  , #token_id .! (#token_id .! token_id)
  )

mkRemove_operator :: Address -> Address -> Natural -> FA.UpdateOperator
mkRemove_operator owner operator token_id =
  FA.Remove_operator
  ( #owner    .! owner
  , #operator .! operator
  , #token_id .! (#token_id .! token_id)
  )

-- =============================================================================
-- Contract storage for testing
-- =============================================================================

-- | No accounts, no operators, no metadata
emptyStorage :: FA.Storage
emptyStorage =
  FA.Storage
    (BigMap Map.empty)
    (BigMap Map.empty)
    (BigMap Map.empty)

-- | Create storage for a given address and total supply. The entirety of the
-- total supply is given to the provided address.
initStorage :: Address -> Natural -> FA.Storage
initStorage addr total =
  FA.Storage
    (BigMap $ Map.fromList [((addr, (#token_id .! 0)), total)])
    (BigMap Map.empty)
    (BigMap Map.empty)

-- | Insert a balance into an existing storage.
insertBalance :: Address -> Natural -> FA.Storage -> FA.Storage
insertBalance addr total (FA.Storage (BigMap ledger) operators metadata) =
  FA.Storage (BigMap $ Map.insert (addr, (#token_id .! 0)) total ledger) operators metadata

-- | Insert an operator into an existing storage.
insertOperator :: Address -> Address -> Natural -> FA.Storage -> FA.Storage
insertOperator owner operator token_id (FA.Storage ledger (BigMap operators) metadata) =
  FA.Storage ledger (BigMap $ Map.insert (owner, (operator, (#token_id .! token_id))) () operators) metadata

-- | Get the an address's balnce.
getBalance :: Address -> FA.Storage -> Maybe Natural
getBalance owner (FA.Storage (BigMap ledger) _ _) =
  Map.lookup (owner, #token_id .! 0) ledger
