{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DeriveAnyClass            #-}
{-# LANGUAGE DeriveGeneric             #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE StandaloneDeriving        #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE UndecidableInstances      #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -fno-warn-orphans      #-}

module FaTwo.Ligo.Contract where

-- references
-- code designed follow tqtezos's stablecoin project
-- https://github.com/tqtezos/stablecoin/blob/master/haskell/src/Lorentz/Contracts/Stablecoin.hs
-- example for cstr and fld
-- https://gitlab.com/morley-framework/morley/-/blob/master/code/cleveland/lorentz-test/Test/Lorentz/CustomValue.hs#L31

import FaTwo.Core
import Data.FileEmbed (embedStringFile)
import FaTwo.Ligo.Path (fa2Path)
import Michelson.Test.Import (readContract)
import Fmt (pretty)
import Lorentz
import qualified Michelson.Typed as T
import Prelude (fail, either, snd)

-- This empty splice lets us workaround the GHC stage restriction, and refer to `Storage`
-- in the TH splices below.
$(pure [])

fa2Contract :: T.Contract (ToT Parameter) (ToT Storage)
fa2Contract =
  -- This TemplateHaskell splice is here to ensure the Michelson representation
  -- (i.e., the generated tree of `or` and `pair`) of these Haskell data
  -- types matches exactly the Michelson representation of the Ligo data types.
  --
  -- For example, if a Ligo data type generates a Michelson balanced tuple like
  -- ((a, b), (c, d)), but the corresponding Haskell data type generates a Michelson
  -- tuple like (a, (b, (c, d))), compilation should fail.
  --
  -- The reason they need to match is because of the way permits work.
  -- If we issue a permit for an entrypoint and the tree of `or`s is incorrect,
  -- then the permit will be unusable.
  --
  -- The splice attempts to parse the fa2.tz contract at compile-time.
  -- If, for example, the Michelson representation of Haskell's
  -- `FaTwo.Ligo.Contract` is different from Ligo's `parameter`,
  -- then this splice will raise a compilation error.
  --
  -- This can usually be fixed by writing a custom instance of Generic using `customGeneric`,
  -- and making sure the data type's layout matches the layout in fa2.tz.
  $(case readContract @(ToT Parameter) @(ToT Storage) fa2Path $(embedStringFile fa2Path) of
      Left e ->
        -- Emit a compiler error if the contract cannot be read.
        fail (pretty e)
      Right _ ->
        -- Emit a haskell expression that reads the contract.
        [|
          -- Note: it's ok to use `error` here, because we just proved that the contract
          -- can be parsed+typechecked.
          either (error . pretty) snd $
            readContract
              fa2Path
              $(embedStringFile fa2Path)
        |]
  )
