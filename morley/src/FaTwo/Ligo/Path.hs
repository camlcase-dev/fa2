-- | This module contains only the paths to the compiled LIGO contracts.
--
-- Due to GHC stage restriction, they have to be in their own module in order
-- to be used inside TemplateHaskell splices.
module FaTwo.Ligo.Path
  ( fa2Path
  ) where

-- | The path to the ligo compiled stablecoin FA2 contract.
fa2Path :: FilePath
fa2Path = "./tests/resources/fa2.tz"
