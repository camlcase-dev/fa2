{-|
Module      : FaTwo.Contract
Copyright   : (c) camlCase, 2020

-- https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md
-- https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-16/tzip-16.md
-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DeriveAnyClass            #-}
{-# LANGUAGE DeriveGeneric             #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE StandaloneDeriving        #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE UndecidableInstances      #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}
{-# OPTIONS_GHC -fno-warn-deprecations #-}
{-# OPTIONS_GHC -fno-warn-orphans      #-}

module FaTwo.Contract where

import FaTwo.Core
import Lorentz

-- =============================================================================
-- Entrypoint: Transfer
-- =============================================================================

defaultOperatorValidator :: Address : Address : TokenId : OperatorStorage : s :-> s
defaultOperatorValidator = do
  dup; sender; eq
  if_
    (do -- sender is owner, no need to check permission
        drop; drop; drop; drop
    )
    (do
        -- prepare the key lookup in OperatorStorage
        -- (owner, (operator, token_id))
        dip pair; pair 
        get
        ifNone
          (do push @MText [mt|FA2_NOT_OPERATOR|]; failWith)
          (drop) -- the stored value is unit, no need to keep it
    )
    

singleTransfer :: Address : TransferDestination : Storage : s :-> Storage : s
singleTransfer = do
  -- defaultOperatorValidator
  duupX @3; toField #operators
  duupX @3; toField #token_id
  sender
  duupX @4
  defaultOperatorValidator

  -- lookup (from_, token_id) in ledger
  duupX @3; toField #ledger
  duupX @3; toField #token_id
  duupX @3
  pair
  get
  
  ifNone
    (do push @MText [mt|FA2_TOKEN_UNDEFINED|]; failWith) -- the (from_, token_id) does not exist
    (do
        -- fromBalance is on the top
        stackType @(Natural : Address : TransferDestination : Storage : _)
        dup
        duupX @4; toField #amount
        assertLe [mt|FA2_INSUFFICIENT_BALANCE|]

        -- calculate new from balance
        -- from_balance - TransferDestination#amount
        stackType @(Natural : Address : TransferDestination : Storage : _)
        duupX @3; toField #amount
        swap
        sub
        abs

        -- update from_ balance
        stackType @(Natural : Address : TransferDestination : Storage : _)
        dip (dip (do dip (do getField #ledger); swap))
        some; swap
        stackType @(Address : Maybe Natural : LedgerStorage : TransferDestination : Storage : _)
        duupX @4; toField #token_id
        swap
        pair
        update

        -- get to_ balance
        stackType @(LedgerStorage : TransferDestination : Storage : _)
        dup
        duupX @3; getField #token_id
        swap
        toField #to_
        pair
        get
        ifNone (push @Natural 0) nop
        
        -- update to_ balance
        stackType @(Natural : LedgerStorage : TransferDestination : Storage : _)
        dig @2; getField #amount; dip (do getField #to_; dip (toField #token_id); pair; swap)
        add
        stackType @(Natural : (Address, TokenId) : LedgerStorage : Storage : _)
        some; swap
        update
        setField #ledger
    )

multipleTransfers :: Transfer : Storage : s :-> Storage : s
multipleTransfers = do
  getField #from_
  swap
  toField #txs
  stackType @([TransferDestination] : Address : Storage : _)

  iter
    (do
        stackType @(TransferDestination : Address : Storage : _)
        swap; dup
        dip singleTransfer

        stackType @(Address : Storage : _)
    )

  stackType @(Address : Storage : _)
  drop

transfer :: Entrypoint [Transfer] Storage
transfer = do
  iter
    (do
        stackType @[Transfer, Storage]
        multipleTransfers
    )
  nil; pair

-- =============================================================================
-- Entrypoint: Balance_of
-- =============================================================================

toBalanceOfResponse :: BalanceOfRequest : Storage : s :-> BalanceOfResponse : Storage : s
toBalanceOfResponse = do
  -- assert that the token_id is 0
  getField #token_id
  push @Natural 0
  coerceWrap
  assertEq [mt|FA2_TOKEN_UNDEFINED|]
  stackType @(BalanceOfRequest : Storage : _)
  
  -- query ledger for owner balance
  getField #owner
  swap
  getField #token_id
  dig @2
  pair
  stackType @((Address, TokenId) : BalanceOfRequest : Storage : _)
  
  duupX @3
  toField #ledger
  swap
  get
  
  ifNone
    (push @Natural 0)
    nop
  stackType @(Natural : BalanceOfRequest : Storage : _)

  -- create the BalanceOfResponse type
  constructT @BalanceOfResponse
     ( fieldCtor $ duupX @2 >> toNamed #request
     , fieldCtor $ dup >> toNamed #balance
     )

  dip (do drop; drop)

balanceOf :: Entrypoint BalanceOfParam Storage
balanceOf = do
  -- get requests, create an empty list of BalanceOfResponse
  getField #requests
  dip (do swap; nil)
  stackType @([BalanceOfRequest] : [BalanceOfResponse] : Storage : BalanceOfParam : _)  

  -- convert BalanceOfRequest to BalanceOfResponse 
  iter
    (do
        stackType @(BalanceOfRequest : [BalanceOfResponse] : Storage : _)
        dip swap
        toBalanceOfResponse
        stackType @(BalanceOfResponse : Storage : [BalanceOfResponse] : _)
        dip swap
        cons
        stackType @([BalanceOfResponse] : Storage : _)
    )

  stackType @([BalanceOfResponse] : Storage : BalanceOfParam : _)  

  -- send BalanceOfResponse to callback
  dip (do swap; toField #callback; push @Mutez minBound)
  transferTokens
  nil; swap; cons; pair
  
-- =============================================================================
-- Entrypoint: Update_operators
-- =============================================================================

updateOperator :: Storage : UpdateOperator : s :-> Storage : s
updateOperator = do
  swap
  caseT @UpdateOperator
    ( #cAdd_operator /-> do
        -- confirm that the sender is the owner
        stackType @(OperatorParam : Storage : _)
        getField #owner
        sender        
        assertEq [mt|FA2_NOT_OWNER|]

        -- setup the operator key for a given (owner, (operator, token_id))
        getField #operator
        swap
        getField #token_id
        dig @2
        pair
        swap
        toField #owner
        pair
        stackType @((Address, (Address, TokenId)) : Storage : _)

        -- get the operators from storage
        dip (getField #operators)

        -- set the value to unit
        unit
        some
        -- put the key on top
        swap
        
        update
        setField #operators
        
        
    , #cRemove_operator /-> do
        stackType @(OperatorParam : Storage : _)
        getField #owner
        sender        
        assertEq [mt|FA2_NOT_OWNER|]

        -- setup the operator key for a given (owner, (operator, token_id))
        getField #operator
        swap
        getField #token_id
        dig @2
        pair
        swap
        toField #owner
        pair
        stackType @((Address, (Address, TokenId)) : Storage : _)

        -- get the operators from storage
        dip (getField #operators)

        -- delete the key from the map
        none
        -- put the key on top
        swap
        
        update
        setField #operators
    )

updateOperators :: Entrypoint [UpdateOperator] Storage
updateOperators = do
  iter
    (do
        stackType @[UpdateOperator, Storage]
        swap
        updateOperator
    )
  nil; pair

-- =============================================================================
-- Contract Code
-- =============================================================================

contractCode :: ContractCode Parameter Storage
contractCode = do
  unpair
  entryCaseSimple @Parameter
    ( #cTransfer         /-> transfer
    , #cBalance_of       /-> balanceOf
    , #cUpdate_operators /-> updateOperators
    )

contract :: Contract Parameter Storage
contract =
  defaultContract $ contractName "fa2" $ do
    contractGeneralDefault
    docStorage @Storage
    contractCode
