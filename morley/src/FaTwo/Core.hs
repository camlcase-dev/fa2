{-|
Module      : FaTwo.Core
Copyright   : (c) camlCase, 2020

-- https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md
-- https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-16/tzip-16.md

Define the Parameter and EntryPoint without typeclass instances
-}

{-# LANGUAGE DataKinds                 #-}
{-# LANGUAGE DeriveAnyClass            #-}
{-# LANGUAGE DeriveGeneric             #-}
{-# LANGUAGE DerivingStrategies        #-}
{-# LANGUAGE DuplicateRecordFields     #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE FlexibleInstances         #-}
{-# LANGUAGE GADTs                     #-}
{-# LANGUAGE MultiParamTypeClasses     #-}
{-# LANGUAGE NoApplicativeDo           #-}
{-# LANGUAGE OverloadedLabels          #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE RankNTypes                #-}
{-# LANGUAGE RebindableSyntax          #-}
{-# LANGUAGE ScopedTypeVariables       #-}
{-# LANGUAGE StandaloneDeriving        #-}
{-# LANGUAGE TemplateHaskell           #-}
{-# LANGUAGE TypeApplications          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE TypeOperators             #-}
{-# LANGUAGE UndecidableInstances      #-}
{-# LANGUAGE ViewPatterns              #-}
{-# OPTIONS_GHC -Wno-unused-do-bind    #-}

module FaTwo.Core where

import Lorentz

-- =============================================================================
-- Transfer
-- =============================================================================

type TokenId =
  ( "token_id" :! Natural
  )

type TransferDestination =
  ( "to_"      :! Address
  , "token_id" :! TokenId
  , "amount"   :! Natural
  )

type Transfer =
  ( "from_" :! Address
  , "txs"   :! [TransferDestination]
  )

-- =============================================================================
-- Balance_of
-- =============================================================================

type BalanceOfRequest =
  ( "owner"    :! Address
  , "token_id" :! TokenId
  )

type BalanceOfResponse =
  ( "request" :! BalanceOfRequest
  , "balance" :! Natural
  )

type BalanceOfParam =
  ( "requests" :! [BalanceOfRequest]
  , "callback" :! ContractRef [BalanceOfResponse]
  )

-- =============================================================================
-- Operator
-- =============================================================================

type OperatorParam =
  ( "owner"    :! Address
  , "operator" :! Address
  , "token_id" :! TokenId
  )

data UpdateOperator
  = Add_operator    OperatorParam
  | Remove_operator OperatorParam  

-- =============================================================================
-- Parameter
-- =============================================================================

data Parameter
  = Transfer         [Transfer]
  | Balance_of       BalanceOfParam
  | Update_operators [UpdateOperator]

-- =============================================================================
-- Storage
-- =============================================================================

type LedgerStorage   = BigMap (Address, TokenId) Natural
type OperatorStorage = BigMap (Address, (Address, TokenId)) () -- (owner, (operator, token_id))
type TokenMetadata   = BigMap TokenId (TokenId, Map MText ByteString) -- "", "decimals", "name", "symbol"

data Storage =
  Storage
    { ledger         :: LedgerStorage 
    , operators      :: OperatorStorage
    , token_metadata :: TokenMetadata
    }

-- | UpdateOperator
customGeneric "UpdateOperator" ligoLayout
deriving anyclass instance IsoValue UpdateOperator
deriving stock instance Show UpdateOperator
deriving anyclass instance HasAnnotation UpdateOperator
instance TypeHasDoc UpdateOperator where
  typeDocMdDescription = "UpdateOperator."
  typeDocHaskellRep = homomorphicTypeDocHaskellRep
  typeDocMichelsonRep = homomorphicTypeDocMichelsonRep

-- | The shape of Parameter must match ligo.
customGeneric "Parameter" rightComb

deriving anyclass instance IsoValue Parameter
deriving stock instance Show Parameter
instance ParameterHasEntrypoints Parameter where
  type ParameterEntrypointsDerivation Parameter = EpdPlain
deriving anyclass instance HasAnnotation Parameter

-- | The shape of Storage must match ligo.
customGeneric "Storage" rightComb
deriving anyclass instance IsoValue Storage
deriving stock instance Show Storage
deriving anyclass instance HasAnnotation Storage
instance TypeHasDoc Storage where
  typeDocMdDescription = "FA2 storage."
  typeDocHaskellRep = homomorphicTypeDocHaskellRep
  typeDocMichelsonRep = homomorphicTypeDocMichelsonRep
