// FA2
// implemented in PascalLigo
// copyright: camlCase 2020
// version: 0.2.0.0
// license: GPLv3

// References

// https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-12/tzip-12.md
// https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-16/tzip-16.md

// =============================================================================
// Transfer
// =============================================================================

type token_id is nat

type transfer_destination is [@layout:comb] record
  to_      : address;
  token_id : token_id;
  amount   : nat;
end

type transfer is [@layout:comb] record
  from_ : address;
  txs   : list(transfer_destination);
end

// =============================================================================
// Balance Of
// =============================================================================

type balance_of_request is [@layout:comb] record
  owner    : address;
  token_id : token_id;
end

type balance_of_response is [@layout:comb] record
  request : balance_of_request;
  balance : nat;
end

type balance_of_param is [@layout:comb] record
  requests : list(balance_of_request);
  callback : contract(list(balance_of_response));
end

// =============================================================================
// Operator
// =============================================================================

type operator_param is [@layout:comb] record
  owner : address;
  operator : address;
  token_id : token_id;
end

type update_operator is [@layout:comb]
| Add_operator     of operator_param
| Remove_operator  of operator_param

// =============================================================================
// Entrypoints
// =============================================================================

type entrypoint is [@layout:comb]
| Transfer                of list(transfer)
| Balance_of              of balance_of_param
| Update_operators        of list(update_operator)

// =============================================================================
// Storage
// =============================================================================

// for simplicity, we are using a simple operator policy. An address either has
// unlimited permission to spend an owner's tokens or they have no permission.

// (owner * (operator * token_id))
type operator_storage is big_map((address * (address * token_id)), unit);

type token_metadata is big_map(nat, (nat * map(string,bytes)));

type storage is [@layout:comb] record
  ledger         : big_map((address * token_id), nat);
  operators      : operator_storage;
  token_metadata : token_metadata;
end;

// =============================================================================
// Type Synonyms
// =============================================================================

const not_operator: string         = "FA2_NOT_OPERATOR";
const not_owner: string            = "FA2_NOT_OWNER";
const insufficient_balance: string = "FA2_INSUFFICIENT_BALANCE";
const token_undefined: string      = "FA2_TOKEN_UNDEFINED";

// =============================================================================
// Entrypoint: Transfer
// =============================================================================

// Check if the operator exists in storage.operator
function default_operator_validator (const owner   : address;
                                     const operator: address;
                                     const token_id: token_id;
                                     const operator_storage: operator_storage) :
                                     unit is
  block {
    if (owner = operator) then skip else {
      case (operator_storage[(owner, (operator, token_id))]) of
        | Some(nothing_) -> skip
        | None        -> failwith(not_operator)
      end
    }
  } with Unit;

// perform a single transfer for a give from_ address and transfer_destination
function transfer_tx (const from_                : address;
                      const transfer_destination : transfer_destination;
                      var storage                : storage) :
                      storage is
 block {
   // check that the operator has permission to transfer the owner's tokens
   default_operator_validator(from_, sender, transfer_destination.token_id, storage.operators);

   if (transfer_destination.token_id = 0n) then {
     case (storage.ledger[(from_, transfer_destination.token_id)]) of
       | Some(from_balance) -> block {
           if (from_balance >= transfer_destination.amount) then {
             // subtract transferred amount from the from_ address balance
             storage.ledger[(from_, transfer_destination.token_id)] := abs(from_balance - transfer_destination.amount);

             // get the to_ address balance
             var to_balance: nat := 0n;
             case (storage.ledger[(transfer_destination.to_, transfer_destination.token_id)]) of
               | Some(to_balance_) -> to_balance := to_balance_
               | None   -> to_balance := 0n
             end;

             // add transferred amount to the to_ address balance
             storage.ledger[(transfer_destination.to_, transfer_destination.token_id)] := to_balance + transfer_destination.amount;
           } else {
             failwith(insufficient_balance)
           };
         }
       | None -> failwith(insufficient_balance)
     end
   } else {
     failwith(token_undefined);
   }
 } with storage;

// validate the operator than execute the transactions 
function transfer_txs (const transfer : transfer;
                       var storage    : storage) :
                       storage is
 block {
   function exec (var storage: storage;
                  const transfer_destination: transfer_destination) :
                  storage is
     transfer_tx(transfer.from_, transfer_destination, storage);
 } with List.fold (exec, transfer.txs, storage);


// Transfer entrypoint function
function transfer (const transfers : list(transfer);
                   var storage     : storage) :
                   storage is
 block {
   function exec (var storage   : storage;
                  const transfer: transfer) :
                  storage is
     transfer_txs(transfer, storage);
 } with List.fold (exec, transfers, storage)

// =============================================================================
// Entrypoint: Balance_of
// =============================================================================

// Balance_of entrypoint function
function balance_of (const balance_of_param : balance_of_param;
                     const storage          : storage) :
                     (list(operation) * storage) is
 block {
   // convert each balance_of
   function to_balance(const balance_of_request : balance_of_request) :
                       balance_of_response is
     block {
       var owner_balance: nat := 0n;     
       if (balance_of_request.token_id =/= 0n)
       then { failwith(token_undefined) }
       else {       
         case storage.ledger[(balance_of_request.owner, balance_of_request.token_id)] of
           | Some(owner_balance_) -> owner_balance := owner_balance_
           | None                 -> skip
         end;       
       };
     } with record [ request = balance_of_request; balance = owner_balance ];
 } with (list [transaction(List.map(to_balance, balance_of_param.requests), amount, balance_of_param.callback)], storage);

// =============================================================================
// Entrypoint: Update_operators
// =============================================================================

// Update_operators entrypoint function
function update_operators (const update_operators : list(update_operator);
                           var   storage          : storage) :
                           storage is
  block {
    function update_operator (var   storage         : storage;
                              const update_operator : update_operator) :
                              storage is
      block {
        case update_operator of
          | Add_operator(operator_param) -> block {
              if sender = operator_param.owner then skip else failwith(not_owner);
              storage.operators[(operator_param.owner, (operator_param.operator, operator_param.token_id))] := Unit
            }
          | Remove_operator(operator_param) -> block {
              if sender = operator_param.owner then skip else failwith(not_owner);
              remove (operator_param.owner, (operator_param.operator, operator_param.token_id)) from map storage.operators
            }
        end
    } with storage;
  } with List.fold(update_operator, update_operators, storage);

// =============================================================================
// Main
// =============================================================================

function main (const entrypoint : entrypoint ; const storage : storage) : (list(operation) * storage) is
  case entrypoint of
  | Transfer(transfers)          -> ((nil : list(operation)), transfer(transfers, storage))
  | Balance_of(balance_of_param) -> balance_of(balance_of_param, storage)
  | Update_operators(xs)         -> ((nil : list(operation)), update_operators(xs, storage))
  end
