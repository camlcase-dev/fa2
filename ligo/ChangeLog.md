# 2020-12-31 -- v0.2.0.0

* Apply [@layout:comb] to parameter and storage.

# 2020-12-31 -- v0.1.0.0

* Implement the required entrypoints for an FA2 contract as a single asset
  contract: transfer, balance_of and update_operators.
